using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Clase singleton, que gracias a incluir la opción que no se destruya en al cargar
//mantiene una única instancia con toda la información de la partida 
//a lo largo de todas las escenas.
public class JuegoControl : MonoBehaviour
{
    public static JuegoControl control;
    public Partida partida = new Partida();  //datos de la partida    
    void Awake(){
        
        if (control == null){
            control = this;
            DontDestroyOnLoad(this.gameObject); 
        } else {
            Destroy(this);
        }              
    }   

 
}