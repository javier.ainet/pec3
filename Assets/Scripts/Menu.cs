using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
Clase con métodos para controlar las acciones de los botones
de la UI del juego (menú, pantalla Game Over y opción salir)
*/

public class Menu : MonoBehaviour
{
   public int opcion;

   public void EmpezarJuego(){ 
       JuegoControl.control.partida.tipoPartida=opcion; 
       SceneManager.LoadScene("Juego");        
   }

   public void MostrarMenu(){
       JuegoControl.control.partida.estado=Partida.Estados.Pausada;
       SceneManager.LoadScene("Menu");
   }

    public void SalirJuego(){
       Application.Quit();
   }

   public void JugarOtraVez(){
       SceneManager.LoadScene("Juego");
   }

    public void MostrarCreditos(){
       SceneManager.LoadScene("Creditos");
   }
}
