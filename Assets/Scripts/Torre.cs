using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
Esta clase controla toda la mecánica del juego y lleva asociado el prefab del proyectil junto con su script.
En la mecánica de la torre se tiene en cuenta factores como si quien dispara es humano u ordenador.
	- Humano: Para desarrollar este sistema se ha empleado un sistema que calcula el ángulo de tiro en base a 
              la posición del ratón respecto a la torre, y una fuerza en base a la distancia con esta. 
    - Ordenador: Las posiciones de disparo ordenador punto disparo (ángulo) y fuerza son aleatorias, pero 
                basadas en la posición de los 2 jugadores estableciendo un valor x aleatorio entre ellos, 
                y una posición y a partir del jugador en posición más elevada.

El script también controla el estado de las animaciones de la torre (espera, disparando, dañada, destruida...).

Al prefab se el ha añadido el componente line renderer que le permite simular una pequeñas estela al proyectil.

Otras cuestiones que controla este script es la actualización de los paneles de los jugadores
*/

public class Torre : MonoBehaviour
{
    public LineRenderer lr;
    public Text velocidad;
    public Text angulo;
    public GameObject bala; //prefab de la bala
    public GameObject circulo; //sprite circulo para la fuerza
    public Animator anim; //controlador animaciones de la torre del jugador
    float distancia=0f;
    public int numJugador; // establecer a que jugador corresponde la torre asociada
    private bool activado; // controlar si es el turno de disparo de esta torre
    private bool computer; //para saber si es una torre controlada por ordenador o humano
    void Start()
    {
       activado=false; 
       computer = JuegoControl.control.partida.jugadores[numJugador].Tipo();  
       anim.SetBool("Danada",false); // hacemos que esté en modo sin daños la animación jugador
       anim.SetBool("Destruida",false);         
    }


    void Update(){        
        //al hacer click disparamos si estamos en turno y somos humanos
        if (Input.GetMouseButtonDown(0) && activado && !JuegoControl.control.partida.jugadores[numJugador].Tipo()){
            anim.SetBool("Dispara",true);                  
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            int ang = Angulo(mousePos,transform.position); 
            GameObject balaInstancia = Instantiate(bala,transform.position,transform.rotation);
            int vel = (int) (distancia*100f)/4; 
            velocidad.text=vel.ToString(); 
            balaInstancia.GetComponent<Rigidbody2D>().velocity=transform.up.normalized * (vel*0.25f);
            AudioSource audio= gameObject.GetComponent<AudioSource>(); 
            audio.PlayOneShot(audio.clip);
            activado=false;  
            JuegoControl.control.partida.jugadores[numJugador].Esperar();                   
        }
         //Al soltar el click entonces deja de animarse el disparo  (cañón de la torre)
         if (Input.GetMouseButtonUp(0)){
            anim.SetBool("Dispara",false); 
         }    

        //No se muestra la linea de dirección si la torre no está en turno
        if (!activado) {
             lr.SetPosition(0,transform.position);
             lr.SetPosition(1,transform.position);
             circulo.transform.localScale = Vector3.zero;
             anim.SetBool("Dispara",false);             
        }

        //comprobar si es nuestro turno entonces se actica
        if (JuegoControl.control.partida.turno==numJugador && JuegoControl.control.partida.jugadores[numJugador].Apuntando()){
            activado=true;
        }    
    }
    void FixedUpdate()
    {
      //activado y humano actualiza distancia mouse y lineas e informacion marcador y movemos torre  
      if (activado && !JuegoControl.control.partida.jugadores[numJugador].Tipo()) {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.rotation = Quaternion.LookRotation(Vector3.forward, mousePos - transform.position); 
            
            if (Distancia(mousePos,transform.position)<=4){
                    distancia=Distancia(mousePos,transform.position); 
                    Vector3 lineaPos=new Vector3(mousePos.x,mousePos.y,0f);
                    lr.SetPosition(1,lineaPos); 
                    circulo.transform.localScale = new Vector3(distancia*2f,distancia*2f,distancia*2f);
                }  else {
                    distancia=4;
                    Vector3 lineaPos=new Vector3(mousePos.x,mousePos.y,0f);
                    lr.SetPosition(1,lineaPos); 
            }    
            int vel = (int) (distancia*100f)/4; 
            InfoDisparo(Angulo(mousePos,transform.position), vel);        
      }

      //activado y es un ordenador quien dispara 
      if (activado  && JuegoControl.control.partida.jugadores[numJugador].Tipo() ) {
          anim.SetBool("Dispara",false);   
          DisparaOrdenador();
      }

      int vidaActual=JuegoControl.control.partida.jugadores[numJugador].MostrarVida();
        if (vidaActual<100) {
            anim.SetBool("Danada",true);
        }

        if (vidaActual<=0) {
            anim.SetBool("Destruida",true);
            gameObject.SetActive(false);
        }
       
    }

   //Método para calcular y ejecutar el disparo del ordenador
   void DisparaOrdenador(){
        float x=0;
        float y=0;
        
        //generamos el vector para apuntar en función de jugador contrario y del propio, y colocamos cañon
        x=Random.Range(JuegoControl.control.partida.jugadores[0].PosicionXJugador(), JuegoControl.control.partida.jugadores[1].PosicionXJugador()); 
        if (JuegoControl.control.partida.jugadores[0].PosicionYJugador() > JuegoControl.control.partida.jugadores[1].PosicionYJugador()){
            y=Random.Range(JuegoControl.control.partida.jugadores[1].PosicionYJugador()+5,JuegoControl.control.partida.jugadores[1].PosicionYJugador()+30);
        } else {
             y=Random.Range(JuegoControl.control.partida.jugadores[0].PosicionYJugador()+5,JuegoControl.control.partida.jugadores[0].PosicionYJugador()+30);
        }

        Vector3 disparoPos = new Vector3(x,y,-20);
        
        distancia = Random.Range(2f,4f);
        int vel = (int) (distancia*100f)/4; 
        InfoDisparo(Angulo(disparoPos,transform.position),vel);  
        
        transform.rotation = Quaternion.LookRotation(Vector3.forward, disparoPos - transform.position);
        
        //disparamos 
        anim.SetBool("Dispara",true);            
        GameObject balaInstancia = Instantiate(bala,transform.position,transform.rotation);
        balaInstancia.GetComponent<Rigidbody2D>().velocity=transform.up.normalized * (vel*0.25f);
        AudioSource audio= gameObject.GetComponent<AudioSource>(); 
        audio.PlayOneShot(audio.clip);
        activado=false;  
        JuegoControl.control.partida.jugadores[numJugador].Esperar();
    }

    //Métodos Auxiliares cálculo e información

    //Método para ángulo entre 2 posiciones (vectores)
    int Angulo (Vector3 P1, Vector3 P2){
        Vector3 vector= P1-P2;
        vector.Normalize();
        float angulo = Mathf.Atan2(vector.y,vector.x)*Mathf.Rad2Deg;
        return (int) angulo;
    }

    //Método auxiliar para calcular distancia entre 2 puntos
    float  Distancia (Vector2 P1, Vector2 P2) {
        return  Vector2.Distance(P1,P2);
    }

    //Método para actualizar en el panel la información del ángulo y velocidad en tiempo de disparo
    void InfoDisparo(int ang, int vel) {
          angulo.text=ang.ToString();
          velocidad.text=vel.ToString();
    }


}
