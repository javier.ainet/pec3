using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
Esta clase se emplea en los objetos que posicionan aleatoriamente a los jugadores en cada partida
para la determinar la ubicación toma como referencia la posición X de la cámara de la escena, 
buscando un valor entre 2 posiciones A y B:
 - Para el de la izquierda A es 24 unidades menos que el centro y B El centro menos 2 unidades
 - Para el de la derecha A es el centro mas 2 unidades y B el centro mas 24 unidades

 NOTA: al asociarse a objeto con componente RigidBody y física cae por si solo desde la parte superior
       del escenario, por lo que sólo es necesario cambiar su posición X
*/

public class Posicionar : MonoBehaviour
{
    public Camera camara;

    public GameObject numeroP;
    public int jugador;
    private float limiteA, limiteB;
    private Rigidbody2D rb;
    float hueco = 24f;
    void Start()
    {
        rb=gameObject.GetComponent<Rigidbody2D>();
        limiteB = camara.transform.position.x;
        if (jugador==0) {
            limiteA = camara.transform.position.x-hueco;
            limiteB = camara.transform.position.x-2f;
        } else {
            limiteA = camara.transform.position.x+2f;
            limiteB = camara.transform.position.x+hueco;
        }       
        this.transform.position= new Vector2(Random.Range(limiteA,limiteB),this.transform.position.y);
    }


      private void OnCollisionEnter2D(Collision2D colision)  {
                 
       //Si colisiona con el suelo, entonces ya podemos posicionar el jugador asociado mediante numJugador
       //y actualizar su posición, pudiendo destruir el objeto actual empleado para mostrar dónde debe empezar           
       if (colision.transform.CompareTag("Suelo")) {
           numeroP.transform.position=new Vector2(this.transform.position.x,this.transform.position.y+0.5f);
           JuegoControl.control.partida.jugadores[jugador].Posicionar(); 
           JuegoControl.control.partida.jugadores[jugador].ActualizarPosicion(numeroP.transform.position);            
           Destroy(gameObject);
        }   
    }
}
