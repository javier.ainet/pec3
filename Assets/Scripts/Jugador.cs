using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
clase que contiene la información de cada jugador, es un modelo que implementa datos 
y varios métodos (set y get) para conocer o actualizar la información del jugador.
	- estado: permite conocer los estados del jugado y su finalidad es la siguiente:
		+ Inicia: conocer que se crea/instancia el objeto de cada jugador que se guarda en PARTIDA.
		+ Posiciona: Conocer cuando a un jugador se le ha asignado ya posición de su torre
		+ Espera: Cuando un jugador no tiene el turno de disparo
		+ Apunta: Cuando el jugador está en disposición del turno y prepara el disparo
		+ Dispara: Cuando el jugador efectua el disparo
	- computer: Identifica si el jugador es una IA del ordenador -> true o si es humano -> false 
	- vida: controla la vida del personaje durante el juego
	- posición: almacena la posición en el mundo del juego.
*/

public class Jugador 
{

   private enum Estados {Inicia,Posiciona,Espera,Apunta,Dispara};

   //propiedades 
   private Estados estado;
   private bool computer; // Computer -> true(default)   Humano -> false 
   private int vida;
   private Vector2 posicion;
   
    //Constructor
   public Jugador () {
       this.computer=true;
       this.vida=100;
       this.estado=Estados.Inicia;
   }

   public void QuitarVida(int cantidad){
       this.vida -= cantidad; 
   }

   public void RellenarVida(int cantidad){
       this.vida = cantidad; 
   }

   public int MostrarVida(){
       return this.vida; 
   }

   public void ModoJugador() {
       this.computer=false;
   }

   public void ModoOrdenador() {
       this.computer=true;
   }
   public void Iniciar() { 
       this.estado=Estados.Inicia;
   }
   public void Posicionar() {
       this.estado=Estados.Posiciona;
   }
   public void Esperar() {
       this.estado=Estados.Espera;
   }
   public void Apuntar() {
       this.estado=Estados.Apunta;
   }

   public void Disparar() {
       this.estado=Estados.Dispara;
   }

   public void ActualizarPosicion(Vector2 posicion) {
       this.posicion=posicion;
   }

   public bool Esperando() {
       return this.estado==Estados.Espera;
   }

   public bool Apuntando() {
       return this.estado==Estados.Apunta;
   }

   public bool Disparando() {
       return this.estado==Estados.Dispara;
   }

   public Vector2 PosicionJugador() {
       return this.posicion;
   }

   public float PosicionXJugador() {
       return this.posicion.x;
   }

    public float PosicionYJugador() {
       return this.posicion.y;
   }
   
   public bool Posicionado() {
       if (this.estado==Estados.Posiciona) {
           return true;
       } else {
           return false;
       }
   }
   public bool Tipo(){
       return this.computer;
   }

   public string Estadojugador(){
       return this.estado.ToString();
   }
   
}

