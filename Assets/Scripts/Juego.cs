using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/*
Script que permite ir comprobando los estados de la partida de tal manera que según estos pueda actuar:
- Pausada: Durante este estado se puede preparar la partida que lleva cuestiones como elegir un escenario al azar, y posicionar a los jugadores en zonas aleatorias de su mitad de juego.
- Preparada: Este estado permite conocer que el escenario y los jugadores ya tienen ubicación, y es en el momento que se despliegan las torres, se muestran los paneles de información de los jugadores y los avatares según su modo de juego.
- Iniciada: Activa el proceso de juego por turnos, asignando el turno al primer jugador.
- Finalizada: Permite conocer en que momento se aplica el fin del juego determinando quien es el vencedor.
*/

public class Juego : MonoBehaviour
{
    public GameObject P1; //Contenedor del jugador 1
    public GameObject P2; //Contenedor del jugador 2
    public GameObject gameOver; //Texto para mostrar GameOver
    public GameObject marcadores; // Accesp al contenedor de los marcadores de los jugaores
    public GameObject escenarios; // Contenedor de los distintos escenarios


    void Awake()
    {
        //Verificamos que la partida esté pausada y escondemos marcadores y jugadores
        if (JuegoControl.control.partida.estado==Partida.Estados.Pausada) {
           marcadores.SetActive(false);  
           P1.SetActive(false);   
           P2.SetActive(false);           
        }   
        //Escemaro Aleatorio
        int numEscenario=Random.Range(0,4);
        CargarEscenario(numEscenario);        
    }

    private void CargarEscenario(int numEscenario)
    {
        //Recorremos los escenarios, dejando solo activo el que se pasa         
        for (int i=0;i<=3;i++){
            if (i==numEscenario){
                escenarios.transform.GetChild(i).gameObject.SetActive(true);
            } else {
                escenarios.transform.GetChild(i).gameObject.SetActive(false);
            }

        }
    }

    void Update(){
        //Partida pausada --> ocultamos los jugadores y los marcadores mientras posicionamos a los jugadores
        if (JuegoControl.control.partida.estado==Partida.Estados.Pausada) {
           marcadores.SetActive(false);  
           P1.SetActive(false);   
           P2.SetActive(false);                             
        }

        //Partida preparada --> Aparece el escanario aleatorio y ya están determinadas las posiciones
        if (JuegoControl.control.partida.estado==Partida.Estados.Preparada) {
           P1.SetActive(true);   
           P2.SetActive(true);
           marcadores.SetActive(true);      
           //Iniciamos la partida            
           JuegoControl.control.partida.IniciarPartida(JuegoControl.control.partida.tipoPartida,JuegoControl.control.partida.turno);         
           ActualizarAvatares();  
        }

        //Partida preparada --> Ha ganado algún jugador, debemos mostrar el game over e ir a la pantalla final
        if (JuegoControl.control.partida.estado==Partida.Estados.Finalizada) {
           gameOver.SetActive(true);   
           Invoke("PantallaFinal",2f);
        }
        
    }

    //Método para poner en el panel informativo el avatar de tipo jugador humano u ordenador
    private void ActualizarAvatares()
    {
        if (JuegoControl.control.partida.jugadores[0].Tipo()){ //es ordenador
           GameObject.Find("IconoP1").GetComponent<SpriteRenderer>().sprite= Resources.Load<Sprite>("iconrobot");
        }
        if (JuegoControl.control.partida.jugadores[1].Tipo()){ //es ordenador
           GameObject.Find("IconoP2").GetComponent<SpriteRenderer>().sprite= Resources.Load<Sprite>("iconrobot");
        }
                
    }

    void LateUpdate() {
        //comprobar si los jugadores estan en su posición y actualizamos el estado de la partida y estado jugadores
        if (JuegoControl.control.partida.jugadores[0].Posicionado() && JuegoControl.control.partida.jugadores[1].Posicionado()){
            JuegoControl.control.partida.estado=Partida.Estados.Preparada;
            JuegoControl.control.partida.jugadores[0].Esperar();
            JuegoControl.control.partida.jugadores[1].Esperar();
        }      
    }

    //Método para cargar la escena de la pantalla final (Game Over)
    void PantallaFinal(){          
         SceneManager.LoadScene("GameOver");
    }

    
}
