using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
Clase que contiene los datos necesarios para gestionar estados de la misma:
	- estado: para conocer los estados de la partida ya explicados.
	- tipoPartida: para conocer.
	- turno: para conocer cual de los 2 jugadores tiene turno de disparo.
	- jugadores: Array con 2 jugadores con sus datos.
	- ganador: almacena quien es el ganador de la partida en curso.

Tiene además los métodos necesarios para su construcción y diferentes funciones como calcular el impacto y actualizar la info jugadores, o cambiar estados de la partida.
*/

public class Partida 
{ 
  public enum Estados {Pausada,Preparada,Iniciada,Finalizada};
  public Estados estado;
  public Jugador[] jugadores = new Jugador[2];
  public int tipoPartida;
  public int turno;
  public int ganador=0;

  //Constructor
  public Partida(){
      this.jugadores[0]=new Jugador();
      this.jugadores[1]=new Jugador();
      this.turno=0;
      this.estado= Estados.Pausada;
      this.tipoPartida=1;      
  }

  //Método para iniciar una partida, indicando el tipo y qué jugador tiene el turno
  //Actualiza la bida de los jugadores.
  public void IniciarPartida(int tipo, int turno){
      if (tipo==1) {
          this.jugadores[0].ModoJugador();
          this.jugadores[1].ModoJugador();
      }

      if (tipo==2) {
          this.jugadores[0].ModoJugador();
          this.jugadores[1].ModoOrdenador();
      }

      if (tipo==3) {
          this.jugadores[0].ModoOrdenador();
          this.jugadores[1].ModoOrdenador();
      }     
       
      this.jugadores[0].RellenarVida(100);
      this.jugadores[1].RellenarVida(100);
      this.jugadores[0].Esperar();
      this.jugadores[1].Esperar();
      this.turno=turno;
      this.jugadores[turno].Apuntar();
      this.tipoPartida=tipo;
      this.estado= Estados.Iniciada;
  }

  //Método para cambiar el turno entre los jugadores
  public void Cambio(){
    if (this.turno==0){
      this.turno=1;      
    } else {
        this.turno=0;
    }
    this.jugadores[turno].Apuntar();
  }

//Este método verifica la distancia del impacto respecto al jugador que lo recibe
//Si la distancia es menor que 2 entonces le resta una cantidad de vida proporcional
//Al final llama al método para comprobar el estado de la partida
  public void ActualizarImpacto(int jugador, Vector2 posicion){
      float distancia = Distancia(posicion,this.jugadores[jugador].PosicionJugador());
      if (distancia<=2f) {
        int dano=(int) (100-(distancia*50));
        this.jugadores[jugador].QuitarVida(dano); 
        ComprobarPartida(jugador); 
      }
  }

  //Método para compobar si el jugador ha perdido toda la vida, implica llamar a final de partida
  private void ComprobarPartida(int jugador)
    {
        if (this.jugadores[jugador].MostrarVida()<=0){
          FinalPartida(jugador);
        }
    }

  //Método axiliar que devuelve la distancia entre 2 posiciones.
  float  Distancia (Vector2 P1, Vector2 P2) {
        return  Vector2.Distance(P1,P2);
    }

  //Método que actualiza la información del jugador que ha ganado y actualiza el estado de la partida
  public void FinalPartida(int jugador){
    if (jugador==0){
      ganador=1;      
    } else {
      ganador=0;
    }
    this.estado=Estados.Finalizada;   
  }

  //Método para pausar la partida
  public void PausarPartida(){
      this.estado=Estados.Pausada;    
    }
  //Método para conocer el estado de la partida
  public Estados EstadoPartida(){
      return this.estado;
  }

}

   
