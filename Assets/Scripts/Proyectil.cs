using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proyectil : MonoBehaviour
{
    public Vector2 velocidad;

    public GameObject explosion;
    public float vel_desp;
    
    Rigidbody2D rb;
    SpriteRenderer sr;
    GameObject exp;
    

    private bool activo; //proyectil está activo, ha sido lanzado
    void Start()
    {
        rb=gameObject.GetComponent<Rigidbody2D>();
        sr=gameObject.GetComponent<SpriteRenderer>();
        activo=true;
        exp=Instantiate(explosion); 
             

    }
    void Update()    
    {
       AudioSource audio=gameObject.GetComponent<AudioSource>();

       //Mientras se mueve actualizamos el ángulo de rotación del proyectil
       if (rb.velocity != Vector2.zero) {
           Vector2 dir = rb.velocity;
           float angulo = Mathf.Atan2(dir.y,dir.x) * Mathf.Rad2Deg -90;
           transform.rotation= Quaternion.AngleAxis(angulo,Vector3.forward);
           //Si el proyectil está por debajo de este valor (fuera del escenario explota)
           if (rb.position.y<=-15) {  
               //Antes de explotar movemos un poco hacia arriba la partícula de explosión para que coincida
               //con el impacto            
               exp.transform.position=new Vector3(transform.position.x, transform.position.y+0.4f,0);
               Explotar(exp);               
           }
       }  

    }

    private void OnCollisionEnter2D(Collision2D colision)  {
      
       AudioSource audio=gameObject.GetComponent<AudioSource>();

       //Si cae al suelo (choca con escenario) debe explotar el proyectil   
       //y actualizar el valor del impacto si procede
       if (colision.transform.CompareTag("Suelo")) {
               rb.velocity=Vector2.zero;                            
               if (activo) {   
                  audio.PlayOneShot(audio.clip);             
                  exp.transform.position=new Vector3(transform.position.x, transform.position.y,0);
                  if (JuegoControl.control.partida.turno==0){
                     JuegoControl.control.partida.ActualizarImpacto(1,transform.position);                                  
                  } else {
                     JuegoControl.control.partida.ActualizarImpacto(0,transform.position);
                  }  
                  Explotar(exp);
               }     
        }
    }

    private void OnTriggerEnter2D(Collider2D colision) {
       AudioSource audio=gameObject.GetComponent<AudioSource>();

       
       //Si entra en colisión con el jugador 1 y es el turno del contrario entonces  
       //explota y actualiza el valor del impacto si procede                
       if (colision.transform.CompareTag("Jugador1") && JuegoControl.control.partida.turno==1 ) {
               rb.velocity=Vector2.zero;                            
               if (activo) { 
                  audio.PlayOneShot(audio.clip);           
                  exp.transform.position=new Vector3(transform.position.x, transform.position.y+0.4f,0);
                  Explotar(exp);
                  JuegoControl.control.partida.ActualizarImpacto(0,transform.position);                                  
               }     
       } 

       
      //Si entra en colisión con el jugador 2 y es el turno del contrario entonces  
       //explota y actualiza el valor del impacto si procede     

       if (colision.transform.CompareTag("Jugador2") && JuegoControl.control.partida.turno==0 ) {
               rb.velocity=Vector2.zero;                            
               if (activo) {
                  audio.PlayOneShot(audio.clip);
                  exp.transform.position=new Vector3(transform.position.x, transform.position.y+0.4f,0);                  
                  JuegoControl.control.partida.ActualizarImpacto(1,transform.position); 
                  Explotar(exp);                                 
               }     
       }  
    }

   //Este método es el encargado de esconder el proyectil si se ha lanzado
   //y mostrar el sistema de partículas de la explosión, destruyendo ambos objetos al final.
    private void Explotar(GameObject exp)
    {
       if (activo) {             
           activo=false; 
           sr.enabled=false;               
           ParticleSystem part=exp.GetComponent<ParticleSystem>();
           part.Play();                                                           
           Destroy(exp,part.main.duration); 
           JuegoControl.control.partida.Cambio();
           Destroy(gameObject,part.main.duration);           
       }
    }
}
