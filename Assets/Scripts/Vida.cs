using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Vida : MonoBehaviour
{
    public Image barra;
    public int jugador;

    int maxVida=100;
    void Start()
    {
        barra.fillAmount = JuegoControl.control.partida.jugadores[jugador].MostrarVida()/maxVida;
        Debug.Log(JuegoControl.control.partida.jugadores[jugador].MostrarVida()/maxVida);
    }

    // Update is called once per frame
    void Update()
    {
        float actualizarVida = Mathf.MoveTowards(barra.fillAmount, (float) JuegoControl.control.partida.jugadores[jugador].MostrarVida()/maxVida, 1f*Time.deltaTime);
        barra.fillAmount = (float) JuegoControl.control.partida.jugadores[jugador].MostrarVida()/maxVida;
    }
}
