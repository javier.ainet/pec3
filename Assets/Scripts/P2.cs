using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Clase que permite controlar que animación aplicar en cada estado del jugador (Player2)
public class P2 : MonoBehaviour
{
   public Animator anim;
    void Start()
    {
        anim.SetBool("Danada",false);
        anim.SetBool("Destruida",false);
    }

    void FixedUpdate()
    {
        int vidaActual=JuegoControl.control.partida.jugadores[1].MostrarVida();
        if (vidaActual<100) {
            anim.SetBool("Danada",true);
        }

        if (vidaActual<=0) {
            anim.SetBool("Destruida",true);
        }
    }    
}
