using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*Clase empleada para actualizar la información de la 
  pantalla final de Game Over con el nombre ganador
  Además permite cambiar el estado de la partida a Pausada*/
public class GameOver : MonoBehaviour
{
   public Text textoGanador; 
    void Start()
    {
        if (JuegoControl.control.partida.ganador==0){
            textoGanador.text="PLAYER 1 - HA GANADO!!!";
        } else{
            textoGanador.text="PLAYER 2 - HA GANADO!!!";
        }
        JuegoControl.control.partida.PausarPartida();
    }
}
