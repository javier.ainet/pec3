# UOC - PROGRAMACIÓN VIDEOJUEGOS 2D - PEC 3
PEC 3  (Opción 2) juego de la asignatura Programación de Videojuegos en 2D (2022) basado en los juegos clásicos Scorched Earth / Worms.

## Resumen
Juego de artillería basado en disparos desde torres de cañones, donde la habilidad consiste en encontrar el ángulo y la fuerza adecuadas para realizar el lanzamiento correcto alcanzando al enemigo. Se pude jugar tanto de 2 personas (turnos alternativos), contra el ordenador o simular una batalla ordenador vs ordenador.


### Vídeo explicativo funcionamiento
https://www.youtube.com/watch?v=yKG62DPPO_A

### Instrucciones
OBJETIVO:
El objetivo es alcanzar al enemigo mediante lanzamiento de proyectiles, debilitando su estructura con cada impacto, cuanto más en el centro del enemigo sea el impacto más vida se le resta. Cada jugador empieza con la vida al 100% y pierde el primero que queda a 0%.

## MODOS DE JUEGO
Los modos de juego que permite son 3:
* Humano vs Humano
En esta modalidad cada jugador podrá realizar un disparo por turnos, tomando el control del ratón para dirigir dicho disparo.

* Humano vs Ordenador
El jugador toma el control del Player1 mientras que el Player2 es controlado por la IA del juego.

* Ordenador vs Ordenador
En este modo cada jugador es controlado por la IA del juego para ir lanzando sucesivos disparos.

## EMPEZAR UNA PARTIDA
En la pantalla MENÚ PRINCIPAL  se debe elegir entre alguno de los 3 modos de juego, tras la selección nos lleva a la pantalla del JUEGO.

## CÓMO DISPARAR
Durante el turno del jugador humano para realizar el disparo debemos apuntar en la dirección deseada, el juego nos mostrará una línea de ayuda de la dirección además de mostrar el ángulo correspondiente en el panel de información, la fuerza máxima de lanzamiento es 100, para ajustar dicha fuerza debemos acercarnos o alejarnos de nuestra torre (se mostrará un círculo visual de dicha fuerza). 

Tras establecer la dirección y el ángulo bastará con pulsar el botón principal del ratón para efectuar el disparo.

NOTA: Cuando disparamos si la bala es nuestra aunque nos caiga encima no nos resta vida.

## PANEL DE INFORMACIÓN 
En el panel de información de cada jugador tenemos información de interés como es una barra de progreso con la vida restante, y los más importante, el ángulo y la velocidad del último tiro efectuado, lo que nos permite afinar en los siguientes intentos en base al resultado anterior.


## GANAR / PERDER UNA PARTIDA
Para ganar la partida basta con impactar las veces necesarias lo más cerca posible del enemigo para ir restando su vida, al poner a 0 la vida del oponente se gana la partida. De igual manera si el oponente vacía nuestro contador a base de impactos se pierde. Tras perder o ganar nos mostrará una pantalla GAME OVER en la que se puede volver al menú 

## SALIR DEL JUEGO
El botón SALIR nos permite abandonar el juego completamente desde las pantallas de MENÚ PRINCIPAL o desde la pantalla GAME OVER. Durente la partida en curso también se puede abandonar y salir al menú principal.


### DESARROLLO E IMPLEMENTACIÓN UNITY
El desarrollo conta de 4 escenas:

01 Menu
Pantalla de inicio del juego que nos permite elegir entre los modos de juego.
En la programación de esta pantalla se crea ya el objeto con el script JuegoControl encargado de mantener los datos de la partida durante todo el juego.

02 Juego
La pantalla del juego se ha diseñado/programado controlada por un GameObject llamado Juego y su Script que permite ir comprobando los estados de la partida de tal manera que según estos pueda actuar:
	
- Pausada: Durante este estado se puede preparar la partida que lleva cuestiones como elegir un escenario al azar, y posicionar a los jugadores en zonas aleatorias de su mitad de juego.
	
- Preparada: Este estado permite conocer que el escenario y los jugadores ya tienen ubicación, y es en el momento que se despliegan las torres, se muestran los paneles de información de los jugadores y los avatares según su modo de juego.
	
- Iniciada: Activa el proceso de juego por turnos, asignando el turno al primer jugador.
	
- Finalizada: Permite conocer en que momento se aplica el fin del juego determinando quien es el vencedor.

	
03 Game Over
Esta pantalla sirve para indicar fin del juego y en ella se muestra la información del jugador vencedor. Tiene una serie de opciones como son volver al menú principal, jugar una revancha mismo modo partida, ver los créditos o salir del juego.

04 Créditos
Mostramos la información de los créditos de los recursos empleados para el desarrollo del juego. 

### Estructuras de Datos (ED)
A continuación se explican las clases más importantes que se han creado y su finalidad en cada parte del juego.

PARTIDA
Contiene los datos necesarios para gestionar estados de la misma:
- estado: para conocer los estados de la partida ya explicados
- tipoPartida: para conocer 	
- turno: para conocer cual de los 2 jugadores tiene turno de disparo
- jugadores: Array con 2 jugadores con sus datos.
- ganador: almacena quien es el ganador de la partida en curso.

Tiene además los métodos necesarios para su construcción y diferentes funciones como calcular el impacto y actualizar la info jugadores, o cambiar estados de la partida.

JUGADOR
Contiene la información de 1 jugador, es un modelo que implementa datos y varios métodos (set y get) para conocer o actualizar la información del jugador.
- estado:permite conocer los estados del jugado y su finalidad es la siguiente:	
	Inicia: conocer que se crea/instancia el objeto de cada jugador que se guarda en PARTIDA.
	Posiciona: Conocer cuando a un jugador se le ha asignado ya posición de su torre
	Espera: Cuando un jugador no tiene el turno de disparo
	Apunta: Cuando el jugador está en disposición del turno y prepara el disparo
	Dispara: Cuando el jugador efectua el disparo

- computer: Identifica si el jugador es una IA del ordenador -> true o si es humano -> false 

- vida: controla la vida del personaje durante el juego

- posición: almacena la posición en el mundo del juego.


TORRE
Controla toda la mecánica del juego y lleva asociado el prefab del proyectil junto con su scruipt.
En la mecánica de la torre se tiene en cuenta factores como si quien dispara es humano u ordenador.
- Humano: Para desarrollar este sistema se ha empleado un sistema que calcula el ángulo de tiro en base a la posición del ratón respecto a la torre, y una fuerza en base a la distancia con esta. 
    
- Ordenador: Las posiciones de disparo ordenador punto disparo (ángulo) y fuerza son aleatorias, pero basadas en la posición de los 2 jugadores estableciendo un valor x aleatorio entre ellos, y una posición y a partir del jugador en posición más elevada.

El script también controla el estado de las animaciones de la torre (espera, disparando, dañada, destruida...).

Al prefab se el ha añadido el componente line renderer que le permite simular una pequeñas estela al proyectil.

Otras cuestiones que controla este script es la actualización de los paneles de los jugadores.

PROYECTIL
El proyectil consta de un prefab y su script. También se le ha asociado un sistema de partículas para generar las explosiones del proyectil, tanto al tocar el suelo como a torre enemiga.

JUEGO
Los script de este objeto se encargan de controlar cuando dibujar el escenario aleatorio, actualizar los avatares y controlar el estado de la partida, todo ello tomando como referencia los estados de la PARTIDA.


## Control del juego entre escenas
Se ha creado un objeto JuegoControl (clase singleton) que se autoinstancia y no se destruye entre escenas para guardar la información de la partida.


## Créditos
*AUTOR: Fco. Javier Feijoo Lopez  (Licencia GNU GPL)

*ENTREGA: PEC3 Videojuegos 2D 2022

*HERRAMIENTAS: Unity 3D v2020.3.0f1

*TIPOGRAFIAS  TTF:  MADE TOMMY y Zone99 -  www.dafont.com

*IMAGENES Y TEXTURA FONDOS/ESCENARIOS:
 - Diseños propios
 - Diseños adaptados y tomados de Tank Constructor (Hippo) de Asset Store Unity
 - Diseños adaptados y tomados de Yughues Free Metal Materials (Nobiax/Yughues) de Asset Store Unity

*EXPLOSIONES:
 - Diseños adaptados de War FX(Jean Moreno) de Asset Store Unity

*AUDIOS: www.freesound.org bajo licencia CC Attribution 3.0
	568857__humanoide9000__military-march-music
	558115__johnnie-holiday__background-music-130bpm
	321215__hybrid-v__sci-fi-weapons-deploy
	kneeling_canon
    399303__deleted-user-5405837__explosion-012

*VÍDEO CRÉDITOS
	Vídeo de blnd en Pexels

